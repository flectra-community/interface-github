# Copyright 2018 Road-Support - Roel Adriaans
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    "name": "Github Connector - OCA extension",
    "summary": "Add OCA specific information to Odoo modules",
    "version": "2.0.1.0.0",
    "category": "Connector",
    "license": "AGPL-3",
    "author": "Odoo Community Association (OCA), Road-Support",
    "website": "https://gitlab.com/flectra-community/interface-github",
    "depends": ["github_connector_flectra"],
    "data": ["views/flectra_module_version.xml"],
    "installable": True,
}
