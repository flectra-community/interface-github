# Copyright (C) 2016-Today: Flectra Community Association (OCA)
# @author: Oscar Alcala (https://twitter.com/oscarolar)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from flectra import fields, models


class FlectraCategory(models.Model):
    _name = "flectra.category"
    _description = "Flectra Category"

    name = fields.Char()
