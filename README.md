# Flectra Community / interface-github

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[github_connector_odoo](github_connector_odoo/) | 2.0.1.0.1| Analyze Odoo modules information from Github repositories
[github_connector](github_connector/) | 2.0.1.0.3| Synchronize information from Github repositories
[github_connector_oca](github_connector_oca/) | 2.0.1.0.0| Add OCA specific information to Odoo modules


